#include <stdlib.h>
#include <stdio.h>

static double const eps1m01 = 1.0 - 0x1P-01;
static double const eps1p01 = 1.0 + 0x1P-01;
static double const eps1m24 = 1.0 - 0x1P-24;
static double const eps1p24 = 1.0 + 0x1P-24;

int main(int argc, char* argv[argc + 1]) {
   for (int i = 1; i < argc; ++i) {
      double const a = strtod(argv[i], 0);
      printf("%d,%g\n",argc, a);
   }
   return EXIT_SUCCESS;
}
